import React from 'react';
import MaterialTable from 'material-table';
import { LOCALIZATION } from '../../utils/constantes';
import Styles from '../../styles/unidadeExecutoraStyles';

export default function EquipeGestoraTable(props) {
    const classes = Styles();

    const colunas = [
        { title: 'ID', field: 'idEquipeGestora', editable: null },
        { title: 'Início', field: 'inicioGestao' },
        { title: 'Fim', field: 'fimGestao' },
        { title: 'Razão Social', field: 'razaoSocial'},
        { title: 'Diretor', field: 'diretor.nome' },
        { title: 'Vice', field: 'viceDiretor.nome' },
        { title: 'Presitente', field: 'presidente.nome' },
        { title: 'Tesoureiro', field: 'tesoureiro.nome' },
        { title: 'Conselheiro 1', field: 'conselheiroFiscal1.nome' },
        { title: 'Conselheiro 2', field: 'conselheiroFiscal2.nome' },
        { title: 'Conselheiro 3', field: 'conselheiroFiscal3.nome' },
    ];

    const options = {
        actionsColumnIndex: -1,
        paging: false,
        search: false,
    };

    return (
        <div className={classes.rootEquipeGestora}>
            <MaterialTable
                title="Equipe Gestora"
                columns={colunas}
                localization={LOCALIZATION}
                data={props.equipeGestora}
                options={options}
                editable={{
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                //
                            }, 600);
                        }),
                }}
            />
        </div>
    );
}