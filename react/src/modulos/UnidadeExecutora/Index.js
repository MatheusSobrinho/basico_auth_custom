import React, { useEffect } from 'react';
import Styles from '../../styles/unidadeExecutoraStyles';
import UnidadeExecutoraRefionalTable from './UnidadeExecutoraRegionalTable';
import { useStateValue } from '../../store/state';
import { buscarTodasUnidadesExecutoras } from '../../services/unidadeExecutoraService';

export default function UnidadeExecutora() {

  const [{ unidadeExecutora }, dispatchUnidadeExecutora] = useStateValue();

  const classes = Styles();

  const buscarTodasUnidadesExecutorasRegionaisAPI = async () => {
    const resposta = await buscarTodasUnidadesExecutoras('R');//Tipo regional
    if (resposta != null) {
      dispatchUnidadeExecutora({
        type: 'inserirUnidadeExecutora',
        data: resposta.data,
      });
    }
  }

  useEffect(() => {
    if (unidadeExecutora.lista.length == 0) {
      buscarTodasUnidadesExecutorasRegionaisAPI();
    }
  }, [unidadeExecutora]);

  return (
    <div className={classes.root}>
      <UnidadeExecutoraRefionalTable />
    </div>
  );
}