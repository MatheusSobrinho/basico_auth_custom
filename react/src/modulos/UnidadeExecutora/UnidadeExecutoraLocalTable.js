import React from 'react';
import Styles from '../../styles/unidadeExecutoraStyles';
import MaterialTable from 'material-table';
import EquipeGestoraTable from './EquipeGestoraTable';
import ContaBancariaTable from './ContaBancariaTable';
import { useStateValue } from '../../store/state';
import { LOCALIZATION } from '../../utils/constantes';
import { inserirUnidadeExecutoraService, 
    alterarUnidadeExecutoraService, 
    deletarUnidadeExecutoraService } 
from '../../services/unidadeExecutoraService';

export default function UnidadeExecutoraLocalTable(props) {

    const classes = Styles();
    const [{ unidadeExecutora }, dispatchUnidadeExecutora] = useStateValue();

    const colunas = [
        { title: 'ID', field: 'idUnidadeExecutora', editable: null },
        { title: 'Escola', field: 'nomeEscolaRegional' },
        { title: 'Nome da UEXL', field: 'nome' },
        { title: 'Endereço', field: 'endereco' },
        { title: 'Unidade Administrativa', field: 'codigoUnidadeAdministrativa' },
        { title: 'Código INEP', field: 'codigoInep' },
        { title: 'CNPJ', field: 'cnpj' },
        { title: 'Ativo', field: 'ativo', type: 'boolean' },
    ];

    const options = {
        actionsColumnIndex: -1,
        pageSize: 10,
        pageSizeOptions: [10, 20, 30],
    };

    const detailPanel = [
        {
            icon: 'people_alt',
            openIcon: 'people_alt',
            tooltip: 'Equipe Gestora',
            render: rowData => {
                // return (
                //     <EquipeGestoraTable equipeGestora={Array(rowData.equipeGestora)} />
                // )
            },
        },
        {
            icon: 'account_balance',
            openIcon: 'account_balance',
            tooltip: 'Contas bancárias',
            render: rowData => {
                return (
                    <ContaBancariaTable contaBancaria={rowData.contaBancaria} />
                )
            },
        },
    ];

    function buscarIdEmLista(dados, idUnidadeExecutora) {
        for (var i = 0; i < dados.length; i++) {
            if (parseInt(dados[i].idUnidadeExecutora) === parseInt(idUnidadeExecutora)) {
                return i;
            }
        }
    }

    const inserirUnidadeExecutoraLocal = async (newData) => {
        const unidadeExecutoraLocal = { ...newData, 
            tipo: "L",
            unidadeExecutoraRegional: {
                idUnidadeExecutora:props.unidadeExecutoraRegionalVinculada
            }
        };
        //console.log(unidadeExecutoraLocal);
        const resposta = await inserirUnidadeExecutoraService(unidadeExecutoraLocal);//Tipo Local
        if (resposta.status == 201) {
            const unidadeExecutoraLocalRetorno = {...unidadeExecutoraLocal, idUnidadeExecutora: resposta.headers['id']};
            const dados = [...unidadeExecutora.lista];
            const idLista = buscarIdEmLista(dados, props.unidadeExecutoraRegionalVinculada);
            const dadosLocaisDeRegional = [...dados[idLista].unidadeExecutoraLocal, unidadeExecutoraLocalRetorno];
            dados[idLista].unidadeExecutoraLocal = dadosLocaisDeRegional;
            dispatchUnidadeExecutora({
                type: 'inserirUnidadeExecutora',
                data: dados,
            });
        }else{
            alert("Ocorreu um erro ao tentar inserir Unidade Executora Local");
        }
    };

    const alterarUnidadeExecutoraLocal = async (newData, oldData) => {
        const unidadeExecutoraComRegional = { ...newData, 
            unidadeExecutoraRegional: {
                idUnidadeExecutora: props.unidadeExecutoraRegionalVinculada
            }
        };
        const resposta = await alterarUnidadeExecutoraService(unidadeExecutoraComRegional);
        if (resposta.status == 204) {
            const dados = [...unidadeExecutora.lista];
            const idLista = buscarIdEmLista(dados, props.unidadeExecutoraRegionalVinculada);
            dados[idLista].unidadeExecutoraLocal[dados[idLista].unidadeExecutoraLocal.indexOf(oldData)] = unidadeExecutoraComRegional;
            dispatchUnidadeExecutora({
                type: 'alterarUnidadeExecutora',
                data: dados,
            });
        }
    };

    const deletarUnidadeExecutoraLocal = async (oldData) => {
        const resposta = await deletarUnidadeExecutoraService(oldData);
        if (resposta.status == 204) {
            const dados = [...unidadeExecutora.lista];
            const idUnidadeExecutoraRegionalVinculada = props.unidadeExecutoraRegionalVinculada;
            const idLista = buscarIdEmLista(dados, idUnidadeExecutoraRegionalVinculada);
            const dadosLocaisDeRegional = [...dados[idLista].unidadeExecutoraLocal];
            dadosLocaisDeRegional.splice(dadosLocaisDeRegional.indexOf(oldData), 1);
            dados[idLista].unidadeExecutoraLocal = dadosLocaisDeRegional;
            dispatchUnidadeExecutora({
                type: 'deletarUnidadeExecutora',
                data: dados,
            });
        }else{
            alert("Ocorreu um erro ao tentar deletar Unidade Executora Local");
        }
    };

    return (
        <div className={classes.rootLocal}>
            <MaterialTable
                title="Unidades Executoras Locais vinculadas"
                columns={colunas}
                data={props.unidadeExecutoraLocal}
                localization={LOCALIZATION}
                options={options}
                detailPanel={detailPanel}
                editable={{
                    onRowAdd: newData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                inserirUnidadeExecutoraLocal(newData);
                            }, 600);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                alterarUnidadeExecutoraLocal(newData, oldData);
                            }, 600);
                        }),
                    onRowDelete: oldData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                deletarUnidadeExecutoraLocal(oldData);
                            }, 600);
                        }),
                }}
            //onRowClick={(event, rowData, togglePanel) => togglePanel()}
            />
        </div>
    );
}