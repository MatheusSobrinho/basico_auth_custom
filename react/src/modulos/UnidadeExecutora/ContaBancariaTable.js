import React from 'react';
import MaterialTable from 'material-table';
import { LOCALIZATION } from '../../utils/constantes';
import Styles from '../../styles/unidadeExecutoraStyles';

export default function ContaBancariaTable(props) {
    const classes = Styles();

    const colunas = [
        { title: 'ID', field: 'idContaBancaria', editable: null },
        { title: 'Nome Referência', field: 'nome'},
        { title: 'Tipo', field: 'tipo' },
        { title: 'Número da conta', field: 'numeroConta'},
        { title: 'Dígito da conta', field: 'digitoConta'},
        { title: 'Número da Agência', field: 'numeroAgencia'},
        { title: 'Dígito da Agência', field: 'digitoAgencia'},
    ];

    console.log(props.contaBancaria);

    const options = {
        actionsColumnIndex: -1,
        paging: false,
        search: false,
    };

    return (
        <div  className={classes.rootContaBancaria}>
            <MaterialTable
                title="Contas Bancárias"
                columns={colunas}
                localization={LOCALIZATION}
                data={props.contaBancaria}
                options={options}
                editable={{
                    onRowAdd: (newData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                //
                            }, 600);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                //
                            }, 600);
                        }),
                    onRowDelete: (oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                                resolve();
                                //
                            }, 600);
                        }),
                }}
            />
        </div>
    );
}