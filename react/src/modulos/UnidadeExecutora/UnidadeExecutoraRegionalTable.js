import React from 'react';
import Styles from '../../styles/unidadeExecutoraStyles';
import MaterialTable from 'material-table';
import UnidadeExecutoraLocalTable from './UnidadeExecutoraLocalTable';
import EquipeGestoraTable from './EquipeGestoraTable';
import ContaBancariaTable from './ContaBancariaTable';
import { useStateValue } from '../../store/state';
import { LOCALIZATION } from '../../utils/constantes';
import {
    inserirUnidadeExecutoraService,
    alterarUnidadeExecutoraService,
    deletarUnidadeExecutoraService
}
    from '../../services/unidadeExecutoraService';

export default function UnidadeExecutoraRegionalTable() {
    const classes = Styles();

    const [{ unidadeExecutora }, dispatchUnidadeExecutora] = useStateValue();

    const colunas = [
        { title: 'ID', field: 'idUnidadeExecutora', editable: null },
        { title: 'Regional', field: 'nomeEscolaRegional' },
        { title: 'Nome da UEXR', field: 'nome' },
        { title: 'Endereço', field: 'endereco' },
        { title: 'Unidade Administrativa', field: 'codigoUnidadeAdministrativa' },
        { title: 'Código INEP', field: 'codigoInep' },
        { title: 'CNPJ', field: 'cnpj' },
        { title: 'Ativo', field: 'ativo', type: 'boolean' },
    ];

    const options = {
        actionsColumnIndex: -1,
        paging: false,
    };

    const detailPanel = [
        {
            tooltip: 'Unidades Locais Vinculadas',
            render: rowData => {
                return (
                    <div>
                        <UnidadeExecutoraLocalTable unidadeExecutoraRegionalVinculada={rowData.idUnidadeExecutora} unidadeExecutoraLocal={rowData.unidadeExecutoraLocal} />
                    </div>
                )
            },
        },
        {
            icon: 'people_alt',
            openIcon: 'people_alt',
            tooltip: 'Equipe Gestora',
            render: rowData => {
                // return (
                //     <EquipeGestoraTable equipeGestora={Array(rowData.equipeGestora)} />
                // )
            },
        },
        {
            icon: 'account_balance',
            openIcon: 'account_balance',
            tooltip: 'Contas bancárias',
            render: rowData => {
                return (
                    <ContaBancariaTable contaBancaria={rowData.contaBancaria} />
                )
            },
        },
    ];

    function buscarIdEmLista(dados, idUnidadeExecutora) {
        for (var i = 0; i < dados.length; i++) {
            if (parseInt(dados[i].idUnidadeExecutora) === parseInt(idUnidadeExecutora)) {
                return i;
            }
        }
    }

    const inserirUnidadeExecutoraRegional = async (newData) => {
        const unidadeExecutoraRegional = { ...newData, tipo: "R" };
        const resposta = await inserirUnidadeExecutoraService(unidadeExecutoraRegional);//Tipo regional
        console.log(resposta);
        if (resposta.status == 201) {
            //Corrigir modelo de inserção, 
            // ele está zerando a lista para obrigar o useEffect a atualizar a lista
            dispatchUnidadeExecutora({
                type: 'inserirUnidadeExecutora',
                data: resposta.data,
            });
        }else{
            alert("Ocorreu um erro ao tentar inserir Unidade Executora");
        }
    };

    const alterarUnidadeExecutoraRegional = async (newData, oldData) => {
        const resposta = await alterarUnidadeExecutoraService(newData);
        if (resposta.status == 204) {
            const dados = [...unidadeExecutora.lista];
            const i = buscarIdEmLista(dados, oldData.idUnidadeExecutora);
            dados[i] = newData;
            dispatchUnidadeExecutora({
                type: 'alterarUnidadeExecutora',
                data: dados,
            });
        }else{
            alert("Ocorreu um erro ao tentar alterar Unidade Executora");
        }
    };

    const deletarUnidadeExecutoraRegional = async (oldData) => {
        const resposta = await deletarUnidadeExecutoraService(oldData);
        if (resposta.status == 204) {
            const dados = [...unidadeExecutora.lista];
            dados.splice(dados.indexOf(oldData), 1);
            dispatchUnidadeExecutora({
                type: 'deletarUnidadeExecutora',
                data: dados,
            });
        }else{
            alert("Ocorreu um erro ao tentar deletar Unidade Executora");
        }
    };

    return (
        <MaterialTable
            title="Unidades Executoras Regionais"
            columns={colunas}
            data={unidadeExecutora.lista}
            localization={LOCALIZATION}
            options={options}
            detailPanel={detailPanel}
            editable={{
                onRowAdd: newData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            inserirUnidadeExecutoraRegional(newData);
                        }, 600);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            console.log(oldData);
                            alterarUnidadeExecutoraRegional(newData, oldData);
                        }, 600);
                    }),
                onRowDelete: oldData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            deletarUnidadeExecutoraRegional(oldData);
                        }, 600);
                    }),
            }}

            onRowClick={(event, rowData, togglePanel) => togglePanel()}
        />
    );
}