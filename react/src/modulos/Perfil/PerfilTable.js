import React from 'react';
import MaterialTable from 'material-table';
import PermissaoTable from './PermissaoTable';
import { useStateValue } from '../../store/state';
import { LOCALIZATION } from '../../utils/constantes';
import { alterarPerfilPermissaoService } from "../../services/permissaoService";
import { alterarPerfilService, deletarPerfilService } from "../../services/perfilService";
import { inserirPerfisService, buscarPerfilPorIdService } from "../../services/perfilService";

export default function PerfilTable(props) {

    const [{ perfil }, dispatchPerfil] = useStateValue();

    const colunas = [
        { title: 'ID', field: 'idPerfil', editable: null },
        { title: 'Nome', field: 'nome' },
        { title: 'Ativo', field: 'ativo', type: 'boolean' },
    ];

    const options = {
        actionsColumnIndex: -1,
        pageSize: 10,
        pageSizeOptions: [10, 20, 30],
    };

    function buscarIdDePerfilEmLista(dados, idPerfil) {
        for (var i = 0; i < dados.length; i++) {
            if (dados[i].idPerfil == idPerfil) {
                return i;
            }
        }
    }
    const inserirPerfil = async (newData) => {
        try {
            const retornoRequest = await inserirPerfisService(newData);
            if (retornoRequest.status === 201) {
                const idPerfil = retornoRequest.headers['id'];
                const retornoRequestPerfilCompleto = await buscarPerfilPorIdService(idPerfil);
                newData = retornoRequestPerfilCompleto.data;
                const dados = [...perfil.lista, newData];
                dispatchPerfil({
                    type: 'inserirPerfil',
                    data: dados,
                })
            }
        } catch (error) {
            alert("Ocorreu um erro ao inserir Perfil!");
        }
    }

    const alterarTemPermissaoPerfil = async (newData, oldData, idPerfil) => {
        const responseRequest = await alterarPerfilPermissaoService(newData);
        try {
            if (responseRequest.status === 204) {
                const dados = [...perfil.lista];
                const i = buscarIdDePerfilEmLista(dados, idPerfil);
                dados[i].perfilPermissao[dados[i].perfilPermissao.indexOf(oldData)] = newData;
                dispatchPerfil({
                    type: "alterarListaPerfilPermissao",
                    data: dados
                })
            }
        } catch (error) {
            alert("Ocorreu um erro ao alterar Permissão!");
        }
    }

    const alterarPerfil = async (newData, oldData) => {
        const responseRequest = await alterarPerfilService(newData);
        try {
            if (responseRequest.status === 204) {
                const dados = [...perfil.lista];
                dados[dados.indexOf(oldData)] = newData;
                dispatchPerfil({
                    type: 'inserirPerfil',
                    data: dados,
                })
            }
        } catch (error) {
            alert("Ocorreu um erro ao alterar Perfil!");
        }
    }

    const deletarPerfil = async (oldData) => {
        const responseRequest = await deletarPerfilService(oldData);
        try {
            if (responseRequest.status === 204) {
                const dados = [...perfil.lista];
                dados.splice(dados.indexOf(oldData), 1);
                dispatchPerfil({
                    type: 'inserirPerfil',
                    data: dados,
                })
            }
        } catch (error) {
            alert("Ocorreu um erro ao alterar Perfil!");
        }
    }

    return (
        <MaterialTable
            title="Perfis"
            columns={colunas}
            data={props.perfil}
            localization={LOCALIZATION}
            options={options}
            editable={{
                onRowAdd: newData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            inserirPerfil(newData);
                        }, 600);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            alterarPerfil(newData, oldData);
                        }, 600);
                    }),
                onRowDelete: oldData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            deletarPerfil(oldData);
                        }, 600);
                    }),
            }}
            detailPanel={rowData => {
                return (
                    <PermissaoTable perfil={rowData} idPerfil={rowData.idPerfil} alterarTemPermissaoPerfil={alterarTemPermissaoPerfil} />
                )
            }}
            onRowClick={(event, rowData, togglePanel) => togglePanel()}
        />
    );
}