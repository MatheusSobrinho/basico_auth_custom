import React, { useState, useEffect } from 'react';
import { useStateValue } from '../../store/state';
import { buscarTodosPerfisService } from '../../services/perfilService';
import Styles from '../../styles/perfilStyles';
import PerfilTable from './PerfilTable';

export default function Perfil() {
  const classes = Styles();
  const [{ perfil }, dispatchPerfil] = useStateValue();

  useEffect(() => {
    if (perfil.lista.length == 0) {
      buscarPerfisAPI();
    }
  }, [perfil.lista]);

  const buscarPerfisAPI = async () => {
    const respostaRequest = await buscarTodosPerfisService();
    dispatchPerfil({
      type: 'inserirPerfil',
      data: respostaRequest.data,
    });
  };

  return (
    <div className={classes.root}>
      <PerfilTable perfil={perfil.lista} />
    </div>
  );
}