export const reducerUnidadeExecutora = (state, action) => {
    switch (action.type) {
        case 'listarUnidadeExecutora':
            return {
                ...state
            };
        case 'inserirUnidadeExecutora':
            return {
                ...state,
                lista: action.data,
            };
        case 'alterarUnidadeExecutora':
            return {
                ...state,
                lista: action.data
            }
        case 'deletarUnidadeExecutora':
            return {
                ...state,
                lista: action.data
            }
        case 'listarUnidadeExecutora':
            return {
                ...state.lista,
            };
        default:
            return state;

    }
};