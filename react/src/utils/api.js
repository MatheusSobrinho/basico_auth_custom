import axios from 'axios';

/* Root */
export const API = `http://localhost:8080`;

/* Login */
export const REST_LOGIN = `/login`;

/* Usuário */
export const REST_USUARIO = `/usuario`;

/* Perfil */
export const REST_PERFIL = `/perfil`;
export const REST_PERFIL_SIMPLES = REST_PERFIL + `/simples`;

/* PerfilPermissao */
export const REST_PERFIL_PERMISSAO = `/perfilpermissao`;

/* UnidadeExecutora */
export const REST_UNIDADE_EXECUTORA = `/unidadeExecutora`;
const tipo = null;
export const REST_UNIDADE_EXECUTORA_TIPO =
    API + REST_UNIDADE_EXECUTORA + `/tipo/${tipo}`;

export const instanceAxios = axios.create({
    baseURL: API,
    timeout: 10000,
    headers: { 'Authorization': localStorage.getItem('Authorization') }
});

export const urlRequest = (action, parametros) => {
    switch (action) {
        // Login
        case 'login':
            return `/login`;

        // Usuario
        case 'usuario':
            return `/usuario`;

        case 'usuarioId':
            return `/usuario/${parametros.id}`;

        // Perfil
        case 'perfil':
            return `/perfil`;

        case 'perfilId':
            return `/perfil/${parametros.id}`;

        case 'perfilSimples':
            return `/perfil/simples`;

        case 'perfilPermissao':
            return `/perfilpermissao`;
        
        // Unidade Executora
        case 'unidadeExecutoraRegional':
            return `/unidadeExecutora/regional`;
            
        case 'unidadeExecutoraRegionalId':
            return `/unidadeExecutora/regional/${parametros.id}`;
        
        case 'unidadeExecutoraLocal':
            return `/unidadeExecutora/local`;

        case 'unidadeExecutoraLocalId':
            return `/unidadeExecutora/local/${parametros.id}`;

        case 'unidadeExecutoraTipo':
            return `/unidadeExecutora/tipo/${parametros.tipo}`;
    }
}


