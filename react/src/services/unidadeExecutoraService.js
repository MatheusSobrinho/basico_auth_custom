import { urlRequest, instanceAxios } from '../utils/api';

export async function buscarTodasUnidadesExecutoras(tipo) {
    return (
        instanceAxios.get(urlRequest("unidadeExecutoraTipo", {
            tipo: tipo
        })).then(response => {
            return response;
        })
    );
}

export async function inserirUnidadeExecutoraService(unidadeExecutora) {
    if (unidadeExecutora.tipo === "R") {
        return (
            instanceAxios.post(urlRequest("unidadeExecutoraRegional"), unidadeExecutora).then(response => {
                return response;
            })
        );
    } else if (unidadeExecutora.tipo === "L") {
        return (
            instanceAxios.post(urlRequest("unidadeExecutoraLocal"), unidadeExecutora).then(response => {
                return response;
            })
        );
    }

}

export async function alterarUnidadeExecutoraService(unidadeExecutora) {
    if (unidadeExecutora.tipo === "R") {
        return (
            instanceAxios.put(urlRequest("unidadeExecutoraRegionalId", { id: unidadeExecutora.idUnidadeExecutora }), unidadeExecutora).then(response => {
                return response;
            })
        );
    } else if (unidadeExecutora.tipo === "L") {
        return (
            instanceAxios.put(urlRequest("unidadeExecutoraLocalId", { id: unidadeExecutora.idUnidadeExecutora }), unidadeExecutora).then(response => {
                return response;
            })
        );
    }

}

export async function deletarUnidadeExecutoraService(unidadeExecutora) {
    if (unidadeExecutora.tipo === "R") {
        return (
            instanceAxios.delete(urlRequest("unidadeExecutoraRegionalId", { id: unidadeExecutora.idUnidadeExecutora })).then(response => {
                return response;
            })
        );
    } else if (unidadeExecutora.tipo === "L") {
        return (
            instanceAxios.delete(urlRequest("unidadeExecutoraLocalId", { id: unidadeExecutora.idUnidadeExecutora })).then(response => {
                return response;
            })
        );
    }

}