import { instanceAxios, urlRequest } from '../utils/api';

export async function buscarTodosUsuariosService() {
  return(
    await instanceAxios.get(urlRequest("usuario")).then(response => {
      return response;
    })
  );
}

export async function inserirUsuarioService(usuario) {
  return(
    await instanceAxios.post(urlRequest("usuario"), usuario).then(response => {
      return response;
    })
  );
}

export async function alterarUsuarioService(usuario) {
  return(
    await instanceAxios.put(urlRequest("usuarioId", {id: usuario.idUsuario}), usuario).then(response => {
      return response;
    })
  );
}

export async function deletarUsuarioService(usuario) {
  return(
    await instanceAxios.delete(urlRequest("usuarioId", {id: usuario.idUsuario})).then(response => {
      return response;
    })
  );
}