import { instanceAxios, urlRequest } from '../utils/api';

export async function buscarTodosPerfisSimplesService() {
  return (
    await instanceAxios.get(urlRequest("perfilSimples")).then(response => {
      return response;
    })
  );
}

export async function buscarTodosPerfisService() {
  return (
    await instanceAxios.get(urlRequest("perfil")).then(response => {
      return response;
    })
  );
}

export async function buscarPerfilPorIdService(idPerfil) {
  return (
    await instanceAxios.get(urlRequest("perfil", {id: idPerfil})).then(response => {
      return response;
    })
  );
}

export async function inserirPerfisService(perfil) {
  return (
    await instanceAxios.post(urlRequest("perfil"), perfil).then(response => {
      return response;
    })
  );
}

export async function alterarPerfilService(perfil) {
  return (
    await instanceAxios.put(urlRequest("perfilId", {id: perfil.idPerfil}), perfil).then(response => {
      return response;
    })
  );
}

export async function deletarPerfilService(perfil) {
  return (
    await instanceAxios.delete(urlRequest("perfilId", {id: perfil.idPerfil})).then(response => {
      return response;
    })
  );
}