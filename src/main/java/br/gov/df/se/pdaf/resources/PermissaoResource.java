package br.gov.df.se.pdaf.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.df.se.pdaf.services.PermissaoService;

/**
 * @author Matheus de Carvalho Sobrinho
 */
@RestController
@RequestMapping(value = "/permissao")
public class PermissaoResource {

	@Autowired
	private PermissaoService permissaoService;

	@GetMapping
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok().body(permissaoService.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		return ResponseEntity.ok().body(permissaoService.findById(id));
	}

}
