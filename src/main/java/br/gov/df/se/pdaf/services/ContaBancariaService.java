package br.gov.df.se.pdaf.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.df.se.pdaf.entities.ContaBancaria;
import br.gov.df.se.pdaf.exceptions.ObjectNotFoundException;
import br.gov.df.se.pdaf.repositories.ContaBancariaRepository;

@Service
public class ContaBancariaService {

	
	@Autowired
	private ContaBancariaRepository contaBancariaRepository;

	public List<ContaBancaria> findAll() {
		return contaBancariaRepository.findAll();
	}
	
	public List<ContaBancaria> findByTipo(Character tipo) {
		return contaBancariaRepository.findByTipo(tipo);
	}

	public ContaBancaria findById(Long id) {
		Optional<ContaBancaria> obj = contaBancariaRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Conta bancária não encontrada"));
	}

}