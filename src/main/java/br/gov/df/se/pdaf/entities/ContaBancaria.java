package br.gov.df.se.pdaf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author Matheus de Carvalho Sobrinho
 */
@Entity
@Table(name = "conta_bancaria", schema = "pdaf")
public class ContaBancaria implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_conta_bancaria")
    private Long idContaBancaria;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "numero_conta")
    private Long numeroConta;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "digito_conta")
    private Character digitoConta;
     
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "numero_agencia")
    private Long numeroAgencia;
    
    @Basic(optional = false)
    @NotNull 
    @Column(name = "digito_agencia")
    private Character digitoAgencia;
    
    @Basic(optional = false)
    @NotNull 
    @Column(name = "tipo")
    private Character tipo;
    
    @JsonIgnore
    @ManyToOne
    private UnidadeExecutoraHistorico unidadeExecutoraHistorico;
    
    @JsonIgnore
    @ManyToOne
    private UnidadeExecutora unidadeExecutora;

	public Long getIdContaBancaria() {
		return idContaBancaria;
	}

	public void setIdContaBancaria(Long idContaBancaria) {
		this.idContaBancaria = idContaBancaria;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getNumeroConta() {
		return numeroConta;
	}

	public void setNumeroConta(Long numeroConta) {
		this.numeroConta = numeroConta;
	}

	public Character getDigitoConta() {
		return digitoConta;
	}

	public void setDigitoConta(Character digitoConta) {
		this.digitoConta = digitoConta;
	}

	public Long getNumeroAgencia() {
		return numeroAgencia;
	}

	public void setNumeroAgencia(Long numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}

	public Character getDigitoAgencia() {
		return digitoAgencia;
	}

	public void setDigitoAgencia(Character digitoAgencia) {
		this.digitoAgencia = digitoAgencia;
	}

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}

	public UnidadeExecutoraHistorico getUnidadeExecutoraHistorico() {
		return unidadeExecutoraHistorico;
	}

	public void setUnidadeExecutoraHistorico(UnidadeExecutoraHistorico unidadeExecutoraHistorico) {
		this.unidadeExecutoraHistorico = unidadeExecutoraHistorico;
	}

	public UnidadeExecutora getUnidadeExecutora() {
		return unidadeExecutora;
	}

	public void setUnidadeExecutora(UnidadeExecutora unidadeExecutora) {
		this.unidadeExecutora = unidadeExecutora;
	}

}
