package br.gov.df.se.pdaf.enums;

public enum UnidadeExecutora {
	
	REGIONAL('R'), 
	LOCAL('L');
    
    private final Character valor;
    
    UnidadeExecutora(Character valorOpcao){
        valor = valorOpcao;
    }
    
    public Character getValor(){
        return valor;
    }
}