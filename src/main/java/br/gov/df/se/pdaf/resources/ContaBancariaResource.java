package br.gov.df.se.pdaf.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.gov.df.se.pdaf.dtos.PerfilDto;
import br.gov.df.se.pdaf.dtos.UnidadeExecutoraLocalDto;
import br.gov.df.se.pdaf.dtos.UnidadeExecutoraRegionalDto;
import br.gov.df.se.pdaf.entities.ContaBancaria;
import br.gov.df.se.pdaf.entities.UnidadeExecutora;
import br.gov.df.se.pdaf.services.ContaBancariaService;
import br.gov.df.se.pdaf.services.UnidadeExecutoraService;

/**
 * @author Matheus de Carvalho Sobrinho
 */
@RestController
@RequestMapping(value = "/contaBancaria")
public class ContaBancariaResource {

	@Autowired
	private ContaBancariaService contaBancariaService;
	
	@GetMapping
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok().body(contaBancariaService.findAll());
	}
	
	@GetMapping("/tipo/{tipo}")
	public ResponseEntity<?> findAllRegional(@PathVariable Character tipo) {
		return ResponseEntity.ok().body(contaBancariaService.findByTipo(tipo));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		ContaBancaria contaBancaria = contaBancariaService.findById(id);
		return ResponseEntity.ok().body(contaBancaria);
	}

//	@PostMapping("/regional")
//	public ResponseEntity<?> inserirRegional(@Valid @RequestBody UnidadeExecutoraRegionalDto unidadeExecutoraRegionalDto) throws Exception {
//		UnidadeExecutora unidadeExecutora = unidadeExecutoraService.inserirRegional(unidadeExecutoraRegionalDto);
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//				.buildAndExpand(unidadeExecutora.getIdUnidadeExecutora()).toUri();
//		HttpHeaders header = new HttpHeaders();
//		header.add("id", unidadeExecutora.getIdUnidadeExecutora().toString());
//		return ResponseEntity.created(uri).headers(header).build();
//	}
//	
//	@PostMapping("/local")
//	public ResponseEntity<?> inserirLocal(@Valid @RequestBody UnidadeExecutoraLocalDto unidadeExecutoraLocalDto) throws Exception {
//		UnidadeExecutora unidadeExecutora = unidadeExecutoraService.inserirLocal(unidadeExecutoraLocalDto);
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//				.buildAndExpand(unidadeExecutora.getIdUnidadeExecutora()).toUri();
//		HttpHeaders header = new HttpHeaders();
//		header.add("id", unidadeExecutora.getIdUnidadeExecutora().toString());
//		return ResponseEntity.created(uri).headers(header).build();
//	}
//	
//	@PutMapping("/regional/{id}")
//	public ResponseEntity<?> alterarRegional(@PathVariable Long id, @Valid @RequestBody UnidadeExecutoraRegionalDto unidadeExecutoraRegionalDto) throws Exception {
//		unidadeExecutoraRegionalDto.setIdUnidadeExecutora(id);
//		unidadeExecutoraService.alterarRegional(unidadeExecutoraRegionalDto);
//		return ResponseEntity.noContent().build();
//	}
//	
//	@PutMapping("/local/{id}")
//	public ResponseEntity<?> alterarLocal(@PathVariable Long id, @Valid @RequestBody UnidadeExecutoraLocalDto unidadeExecutoraLocalDto) throws Exception {
//		unidadeExecutoraLocalDto.setIdUnidadeExecutora(id);
//		unidadeExecutoraService.alterarLocal(unidadeExecutoraLocalDto);
//		return ResponseEntity.noContent().build();
//	}
//
//	@DeleteMapping("/regional/{id}")
//	public ResponseEntity<?> deletarRegional(@PathVariable Long id) throws Exception {
//		unidadeExecutoraService.deletar(id);
//		return ResponseEntity.noContent().build();
//	}
//
//	@DeleteMapping("/local/{id}")
//	public ResponseEntity<?> deletarLocal(@PathVariable Long id) throws Exception {
//		unidadeExecutoraService.deletar(id);
//		return ResponseEntity.noContent().build();
//	}
	
}
