package br.gov.df.se.pdaf.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.gov.df.se.pdaf.entities.ContaBancaria;
import br.gov.df.se.pdaf.entities.UnidadeExecutora;

/**
* @author Matheus de Carvalho Sobrinho
*/
@Repository
public interface ContaBancariaRepository extends JpaRepository<ContaBancaria, Long>{
	public List<ContaBancaria> findAll();
	public Optional<ContaBancaria> findById(Long id);
	public List<ContaBancaria> findByTipo(Character tipo);
	public List<ContaBancaria> findByUnidadeExecutoraAndTipo(UnidadeExecutora unidadeExecutora, Character tipo);
}
