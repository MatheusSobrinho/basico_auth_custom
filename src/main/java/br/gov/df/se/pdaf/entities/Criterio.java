package br.gov.df.se.pdaf.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
*
* @author Matheus de Carvalho Sobrinho
* @author Vinícius Orrú
*/
@Entity
@Table(name = "criterio", schema = "pdaf")
public class Criterio implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_criterio")
    private Long idCriterio;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private Character tipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "incidencia")
    private Character incidencia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ativo")
    private Boolean ativo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "criterio")
    private List<UnidadeCriterio> unidadeCriterio;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "criterio")
    private List<CriterioIntervalo> criterioIntervalo;

	public Long getIdCriterio() {
		return idCriterio;
	}

	public void setIdCriterio(Long idCriterio) {
		this.idCriterio = idCriterio;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}

	public Character getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(Character incidencia) {
		this.incidencia = incidencia;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public List<UnidadeCriterio> getUnidadeCriterio() {
		return unidadeCriterio;
	}

	public void setUnidadeCriterio(List<UnidadeCriterio> unidadeCriterio) {
		this.unidadeCriterio = unidadeCriterio;
	}

	public List<CriterioIntervalo> getCriterioIntervalo() {
		return criterioIntervalo;
	}

	public void setCriterioIntervalo(List<CriterioIntervalo> criterioIntervalo) {
		this.criterioIntervalo = criterioIntervalo;
	}
    
}
