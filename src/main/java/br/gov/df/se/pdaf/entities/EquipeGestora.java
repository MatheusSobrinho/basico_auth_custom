package br.gov.df.se.pdaf.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
*
* @author Matheus de Carvalho Sobrinho
*/
@Entity
@Table(name = "equipe_gestora", schema = "pdaf")
public class EquipeGestora implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_equipe_gestora")
    private Long idEquipeGestora ;
    
    @NotNull
    @OneToOne
    private Usuario diretor;
     
    @NotNull
    @OneToOne
    private Usuario viceDiretor;
    
    @NotNull
    @OneToOne
    private Usuario tesoureiro;
    
    @NotNull
    @OneToOne
    private Usuario conselheiroFiscal1;
    
    @NotNull
    @OneToOne
    private Usuario conselheiroFiscal2;
    
    @NotNull
    @OneToOne
    private Usuario conselheiroFiscal3;
    
    @NotNull
    @OneToOne
    private Usuario presidente;

    @NotNull
    @Column(name = "razao_social")
    private String razaoSocial;
    
    private Date inicioGestao;
    
    private Date fimGestao;

	public Long getIdEquipeGestora() {
		return idEquipeGestora;
	}

	public void setIdEquipeGestora(Long idEquipeGestora) {
		this.idEquipeGestora = idEquipeGestora;
	}

	public Usuario getDiretor() {
		return diretor;
	}

	public void setDiretor(Usuario diretor) {
		this.diretor = diretor;
	}

	public Usuario getViceDiretor() {
		return viceDiretor;
	}

	public void setViceDiretor(Usuario viceDiretor) {
		this.viceDiretor = viceDiretor;
	}

	public Usuario getTesoureiro() {
		return tesoureiro;
	}

	public void setTesoureiro(Usuario tesoureiro) {
		this.tesoureiro = tesoureiro;
	}

	public Usuario getConselheiroFiscal1() {
		return conselheiroFiscal1;
	}

	public void setConselheiroFiscal1(Usuario conselheiroFiscal1) {
		this.conselheiroFiscal1 = conselheiroFiscal1;
	}

	public Usuario getConselheiroFiscal2() {
		return conselheiroFiscal2;
	}

	public void setConselheiroFiscal2(Usuario conselheiroFiscal2) {
		this.conselheiroFiscal2 = conselheiroFiscal2;
	}

	public Usuario getConselheiroFiscal3() {
		return conselheiroFiscal3;
	}

	public void setConselheiroFiscal3(Usuario conselheiroFiscal3) {
		this.conselheiroFiscal3 = conselheiroFiscal3;
	}

	public Usuario getPresidente() {
		return presidente;
	}

	public void setPresidente(Usuario presidente) {
		this.presidente = presidente;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public Date getInicioGestao() {
		return inicioGestao;
	}

	public void setInicioGestao(Date inicioGestao) {
		this.inicioGestao = inicioGestao;
	}

	public Date getFimGestao() {
		return fimGestao;
	}

	public void setFimGestao(Date fimGestao) {
		this.fimGestao = fimGestao;
	}
    
} 
