package br.gov.df.se.pdaf.enums;

public enum ContaBancariaEnum {
	
	CORRENTE('C'), 
	APLICACAO('A');
    
    private final Character valor;
    
    ContaBancariaEnum(Character valorOpcao){
        valor = valorOpcao;
    }
    
    public Character getValor(){
        return valor;
    }
}