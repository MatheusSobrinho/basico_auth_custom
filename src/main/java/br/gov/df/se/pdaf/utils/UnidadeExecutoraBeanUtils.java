package br.gov.df.se.pdaf.utils;

import org.springframework.beans.BeanUtils;

import br.gov.df.se.pdaf.dtos.UnidadeExecutoraLocalDto;
import br.gov.df.se.pdaf.dtos.UnidadeExecutoraRegionalDto;
import br.gov.df.se.pdaf.entities.UnidadeExecutora;

public class UnidadeExecutoraBeanUtils {

	public UnidadeExecutoraBeanUtils() {
	}

	public static UnidadeExecutora copyProperties(UnidadeExecutoraRegionalDto unidadeExecutoraRegionalDto) {
		UnidadeExecutora unidadeExecutora = new UnidadeExecutora();
		BeanUtils.copyProperties(unidadeExecutoraRegionalDto, unidadeExecutora);
		return unidadeExecutora;
	}
	
	public static UnidadeExecutora copyProperties(UnidadeExecutoraLocalDto unidadeExecutoraLocalDto) {
		UnidadeExecutora unidadeExecutora = new UnidadeExecutora();
		BeanUtils.copyProperties(unidadeExecutoraLocalDto, unidadeExecutora);
		return unidadeExecutora;
	}
	
}