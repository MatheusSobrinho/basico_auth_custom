package br.gov.df.se.pdaf.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.gov.df.se.pdaf.dtos.UnidadeExecutoraLocalDto;
import br.gov.df.se.pdaf.dtos.UnidadeExecutoraRegionalDto;
import br.gov.df.se.pdaf.entities.Perfil;
import br.gov.df.se.pdaf.entities.UnidadeExecutora;
import br.gov.df.se.pdaf.exceptions.ObjectNotFoundException;
import br.gov.df.se.pdaf.repositories.UnidadeExecutoraRepository;
import br.gov.df.se.pdaf.utils.UnidadeExecutoraBeanUtils;

@Service
public class UnidadeExecutoraService {

	
	@Autowired
	private UnidadeExecutoraRepository unidadeExecutoraRepository;

	public List<Perfil> perfisRetorno;

	public List<UnidadeExecutora> findAll() {
		return unidadeExecutoraRepository.findAll();
	}
	
	public List<UnidadeExecutora> findByTipo(Character tipo) {
		return unidadeExecutoraRepository.findByTipo(tipo);
	}

	public UnidadeExecutora findById(Long id) {
		Optional<UnidadeExecutora> obj = unidadeExecutoraRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("unidade executora não encontrada"));
	}

	@Transactional(rollbackFor = Exception.class)
	public UnidadeExecutora inserirRegional(UnidadeExecutoraRegionalDto unidadeExecutoraRegionalDto) {
		return unidadeExecutoraRepository.save(UnidadeExecutoraBeanUtils.copyProperties(unidadeExecutoraRegionalDto));
	}

	@Transactional(rollbackFor = Exception.class)
	public UnidadeExecutora inserirLocal(UnidadeExecutoraLocalDto unidadeExecutoraLocalDto) {
		UnidadeExecutora unidadeExecutoraRegional = unidadeExecutoraRepository.getOne(unidadeExecutoraLocalDto.getUnidadeExecutoraRegional().getIdUnidadeExecutora());
		UnidadeExecutora unidadeExecutoraSalvar = UnidadeExecutoraBeanUtils.copyProperties(unidadeExecutoraLocalDto);
		unidadeExecutoraSalvar.setUnidadeExecutoraRegional(unidadeExecutoraRegional);
		return unidadeExecutoraRepository.save(unidadeExecutoraSalvar);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public UnidadeExecutora alterarRegional(UnidadeExecutoraRegionalDto unidadeExecutoraRegionalDto) {
		UnidadeExecutora unidadeExecutoraSalvar = UnidadeExecutoraBeanUtils.copyProperties(unidadeExecutoraRegionalDto);
		return unidadeExecutoraRepository.save(unidadeExecutoraSalvar);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public UnidadeExecutora alterarLocal(UnidadeExecutoraLocalDto unidadeExecutoraLocalDto) {
		UnidadeExecutora unidadeExecutoraSalvar = UnidadeExecutoraBeanUtils.copyProperties(unidadeExecutoraLocalDto);
		return unidadeExecutoraRepository.save(unidadeExecutoraSalvar);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletar(Long idUnidadeExecutora) {
		UnidadeExecutora unidadeExecutora = findById(idUnidadeExecutora);
		unidadeExecutoraRepository.delete(unidadeExecutora);
	}

}