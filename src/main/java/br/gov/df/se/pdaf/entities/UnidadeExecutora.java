package br.gov.df.se.pdaf.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
*
* @author Matheus de Carvalho Sobrinho
*/
@Entity
@Table(name = "unidade_executora", schema = "pdaf")
public class UnidadeExecutora implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_unidade_executora")
    private Long idUnidadeExecutora;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nome")
    private String nome;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nome_escola_regional")
    private String nomeEscolaRegional;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipo")
    private Character tipo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "cnpj")
    private Long cnpj;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "endereco")
    private String endereco;
    
    /**
     * Observar que Unidade Executora REGIONAL não possui código inep
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo_inep")
    private long codigoInep;
    
    /**
     * Deverá ser feita consulta no sigep
     */
    //@Basic(optional = false)
    //@NotNull
    @Column(name = "codigo_unidade_administrativa")
    private Long codigoUnidadeAdministrativa;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ativo")
    private boolean ativo;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeExecutora", fetch = FetchType.LAZY)
    private List<UnidadeCriterio> unidadeCriterio;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeExecutora", fetch = FetchType.LAZY)
    private List<UnidadeExecutoraHistorico> unidadeExecutoraHistorico;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeExecutora", fetch = FetchType.LAZY)
    private List<ContaBancaria> contaBancaria;
    
    @JsonIgnore
    @OneToOne
    private EquipeGestora equipeGestora;
    
    @JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_unidade_executora_regional", referencedColumnName="id_unidade_executora")
	private UnidadeExecutora unidadeExecutoraRegional;
    
	@OneToMany(mappedBy="unidadeExecutoraRegional", fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
	private List<UnidadeExecutora> unidadeExecutoraLocal = new ArrayList<UnidadeExecutora>();
    
	public UnidadeExecutora() {}
    
	public List<ContaBancaria> getContaBancaria() {
		return contaBancaria;
	}

	public void setContaBancaria(List<ContaBancaria> contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	public UnidadeExecutora getUnidadeExecutoraRegional() {
		return unidadeExecutoraRegional;
	}

	public void setUnidadeExecutoraRegional(UnidadeExecutora unidadeExecutoraRegional) {
		this.unidadeExecutoraRegional = unidadeExecutoraRegional;
	}

	public List<UnidadeExecutora> getUnidadeExecutoraLocal() {
		return unidadeExecutoraLocal;
	}

	public void setUnidadeExecutoraLocal(List<UnidadeExecutora> unidadeExecutoraLocal) {
		this.unidadeExecutoraLocal = unidadeExecutoraLocal;
	}

	public Long getIdUnidadeExecutora() {
		return idUnidadeExecutora;
	}

	public void setIdUnidadeExecutora(Long idUnidadeExecutora) {
		this.idUnidadeExecutora = idUnidadeExecutora;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeEscolaRegional() {
		return nomeEscolaRegional; 
	}

	public void setNomeEscolaRegional(String nomeEscolaRegional) {
		this.nomeEscolaRegional = nomeEscolaRegional;
	}

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getCodigoInep() {
		return codigoInep;
	}

	public void setCodigoInep(long codigoInep) {
		this.codigoInep = codigoInep;
	}

	public Long getCodigoUnidadeAdministrativa() {
		return codigoUnidadeAdministrativa;
	}

	public void setCodigoUnidadeAdministrativa(Long codigoUnidadeAdministrativa) {
		this.codigoUnidadeAdministrativa = codigoUnidadeAdministrativa;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<UnidadeCriterio> getUnidadeCriterio() {
		return unidadeCriterio;
	}

	public void setUnidadeCriterio(List<UnidadeCriterio> unidadeCriterio) {
		this.unidadeCriterio = unidadeCriterio;
	}
	
	public EquipeGestora getEquipeGestora() {
		return equipeGestora;
	}

	public void setEquipeGestora(EquipeGestora equipeGestora) {
		this.equipeGestora = equipeGestora;
	}

	public List<UnidadeExecutoraHistorico> getUnidadeExecutoraHistorico() {
		return unidadeExecutoraHistorico;
	}

	public void setUnidadeExecutoraHistorico(List<UnidadeExecutoraHistorico> unidadeExecutoraHistorico) {
		this.unidadeExecutoraHistorico = unidadeExecutoraHistorico;
	}

}
