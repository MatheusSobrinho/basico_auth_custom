package br.gov.df.se.pdaf.dtos;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import br.gov.df.se.pdaf.entities.ContaBancaria;
import br.gov.df.se.pdaf.entities.EquipeGestora;
import br.gov.df.se.pdaf.entities.UnidadeExecutora;

/**
*
* @author Matheus de Carvalho Sobrinho
*/

public class UnidadeExecutoraLocalDto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long idUnidadeExecutora;
    @NotNull
    private String nome;
    @NotNull
    private String nomeEscolaRegional;
    @NotNull
    private Character tipo;
    @NotNull
    private Long cnpj;
    @NotNull
    private String endereco;
    
    /**
     * Observar que Unidade Executora REGIONAL não possui código inep
     */
    @NotNull
    private long codigoInep;
    
    /**
     * Deverá ser feita consulta no sigep ou Sigrh
     */
    private Long codigoUnidadeAdministrativa;
    private boolean ativo;
    
    private List<ContaBancaria> contaBancaria;
    
    private EquipeGestora equipeGestora;
    
    /**
     * Auto relacionamento
     */
    @NotNull
	private UnidadeExecutora unidadeExecutoraRegional;  
	
	public List<ContaBancaria> getContaBancaria() {
		return contaBancaria;
	}

	public void setContaBancaria(List<ContaBancaria> contaBancaria) {
		this.contaBancaria = contaBancaria;
	}

	public UnidadeExecutora getUnidadeExecutoraRegional() {
		return unidadeExecutoraRegional;
	}

	public void setUnidadeExecutoraRegional(UnidadeExecutora unidadeExecutoraRegional) {
		this.unidadeExecutoraRegional = unidadeExecutoraRegional;
	}

	public Long getIdUnidadeExecutora() {
		return idUnidadeExecutora;
	}

	public void setIdUnidadeExecutora(Long idUnidadeExecutora) {
		this.idUnidadeExecutora = idUnidadeExecutora;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeEscolaRegional() {
		return nomeEscolaRegional;
	}

	public void setNomeEscolaRegional(String nomeEscolaRegional) {
		this.nomeEscolaRegional = nomeEscolaRegional;
	}

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}

	public Long getCnpj() {
		return cnpj;
	}

	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getCodigoInep() {
		return codigoInep;
	}

	public void setCodigoInep(long codigoInep) {
		this.codigoInep = codigoInep;
	}

	public Long getCodigoUnidadeAdministrativa() {
		return codigoUnidadeAdministrativa;
	}

	public void setCodigoUnidadeAdministrativa(Long codigoUnidadeAdministrativa) {
		this.codigoUnidadeAdministrativa = codigoUnidadeAdministrativa;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public EquipeGestora getEquipeGestora() {
		return equipeGestora;
	}

	public void setEquipeGestora(EquipeGestora equipeGestora) {
		this.equipeGestora = equipeGestora;
	}

}
