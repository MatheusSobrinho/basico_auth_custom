package br.gov.df.se.pdaf.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
*
* @author Matheus de Carvalho Sobrinho
*/
@Entity
@Table(name = "unidade_executora_historico", schema = "pdaf")
public class UnidadeExecutoraHistorico implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_unidade_executora_historico")
    private Long idUnidadeExecutoraHistorico;//TODO: mudar para id_unidade_dados_ano_execucao no banco de dados
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "digito_conta")
    private Character digitoConta;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "conta")
    private Integer conta;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "digito_agencia")
    private Character digitoAgencia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "agencia")
    private Integer agencia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "terceirizado")
    private Boolean terceirizado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "qtd_base")
    private Integer qtdBase;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeExecutoraHistorico")
    private List<ContaBancaria> contaBancaria;
    
    @JoinColumn(name = "id_unidade_executora", referencedColumnName = "id_unidade_executora")
    @ManyToOne(optional = false)
    private UnidadeExecutora unidadeExecutora;
    
}
