package br.gov.df.se.pdaf.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
*
* @author Matheus de Carvalho Sobrinho
*/
@Entity
@Table(name = "recurso_financeiro", schema = "pdaf")
public class RecursoFinanceiro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_recurso_financeiro")
    private Long idRecursoFinanceiro;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ano_execucao")
    @Size(min = 4, max = 4)
    private String anoExecucao;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "semestre")
    private Character semestre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private Double valor;

}
